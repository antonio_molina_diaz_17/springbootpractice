package com.domain.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.domain.modelo.Alumno;
import com.domain.modelo.Model;

import util.ConnectionManager;

public class AlumnoDao implements DAO {

	public AlumnoDao() {
		
	}

	@Override
	public void agregar(Model pModel) throws ClassNotFoundException, SQLException {
		// conectar a la bbdd
		ConnectionManager.conectar();
		// obtengo conexión
		Connection con = ConnectionManager.getConexion();
		// sql
		StringBuilder sql = new StringBuilder("insert into alumnos(alu_nombre,alu_apellido, alu_email, alu_conocimientos,alu_git,alu_observaciones");
		sql.append("values(?,?,?,?,?,?)");
		// downCast para recuperar objeto
		Alumno alu = (Alumno) pModel;
		PreparedStatement ps = con.prepareStatement(sql.toString());
		ps.setString(1, alu.getNombre());
		ps.setString(2, alu.getApellido());
		ps.setString(3, alu.getEmail());
		ps.setString(4, alu.getEstudios());
		ps.setString(5, alu.getLinkArepositorio());
		ps.setString(6, alu.getObservaciones());
		// ejecución
		ps.execute();
		// cerrar conexión
		ps.close();
		ConnectionManager.desconectar();
		
	}

	@Override
	public void modificar(Model pModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void eliminar(Model pModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Model> leer(Model pModel) {
		// TODO Auto-generated method stub
		return null;
	}

}
