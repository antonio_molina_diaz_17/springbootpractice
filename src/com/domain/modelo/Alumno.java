package com.domain.modelo;

import java.util.Objects;

public class Alumno implements Model, Vaciable {

	// atributos
	private int codigo;
	private String nombre;
	private String apellido;
	private String estudios;
	private String email;
	private String observaciones;
	private String linkArepositorio;
	public Alumno() {
		// TODO Auto-generated constructor stub
	}

	public Alumno(int codigo, String nombre, String apellido, String estudios, String linkArepositorio, String email, String observaciones) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.email  = email;
		this.observaciones = observaciones;
		this.apellido = apellido;
		this.estudios = estudios;
		this.linkArepositorio = linkArepositorio;
	}
	
	//gettter y setters
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEstudios() {
		return estudios;
	}

	public void setEstudios(String estudios) {
		this.estudios = estudios;
	}

	public String getLinkArepositorio() {
		return linkArepositorio;
	}

	public void setLinkArepositorio(String linkArepositorio) {
		this.linkArepositorio = linkArepositorio;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Override
	public int hashCode() {
		return Objects.hash(apellido, codigo, nombre);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Alumno))
			return false;
		Alumno other = (Alumno) obj;
		return Objects.equals(apellido, other.apellido) && codigo == other.codigo
				&& Objects.equals(nombre, other.nombre);
	}

	@Override
	public String toString() {
		// StringBuffer pensada para multihilos
		// StringBuilder no es multihilo
		StringBuilder sb = new StringBuilder("codigo=");
		sb.append(codigo);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", apellido=");
		sb.append(apellido);
		sb.append(", email=");
		sb.append(email);
		sb.append(", estudios=");
		sb.append(estudios);
		sb.append(", linkArepositorio=");
		sb.append(linkArepositorio);
		sb.append(", observaciones=");
		sb.append(observaciones);
		return sb.toString();
		/*
		 * mas r�pido que el autom�tico
		return "Alumno [codigo=" + codigo + ", nombre=" + nombre + ", apellido=" + apellido + ", estudios=" + estudios
				+ ", linkArepositorio=" + linkArepositorio + "]";
	*/
	}
	
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return codigo == 0 && 
				(nombre == null || nombre.isEmpty() )
				&& (apellido == null || apellido.isEmpty() )
				&& (email == null || email.isEmpty() )
				&& (estudios == null || estudios.isEmpty() )
				&& (linkArepositorio == null || linkArepositorio.isEmpty() )
				&& (observaciones == null || observaciones.isEmpty() )
				;
	}
	
	

}
