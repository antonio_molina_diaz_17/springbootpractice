package util.test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import util.ConnectionManager;

class ConnectionManagerTest {

	@Test
	public void testConectar()  {
		try {
			 ConnectionManager.conectar();
			 assertTrue(true);
		}catch (Exception e) {
			assertTrue(false);
		}		
	}
	
	@Test
	public void testDesConectar()  {
		try {
			 ConnectionManager.desconectar();
			 assertTrue(true);
		}catch (Exception e) {
			assertTrue(false);
		}		
	}

}
