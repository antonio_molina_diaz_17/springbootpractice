package es.com.inetum.elementos.modelo;

public class Papel extends ElementoFactory {

	public Papel() {
		super("PAPEL", 1);
	}

	@Override
	public int comparar(ElementoFactory pElem) {
		int numero                  = pElem.getNumero();
		int resultado               = 0;
		if(numero == PIEDRA) {
			resultado = 1;
		    descripcionResultado = "papel le gan� a Piedra";
		}
		else if(numero == TIJERAS) {
			resultado = -1;
			descripcionResultado = "papel pierde con tijeras";
		}
		else {
			resultado = 0;
			descripcionResultado = "empate";
		}
				
		return resultado;
		
	}

}
