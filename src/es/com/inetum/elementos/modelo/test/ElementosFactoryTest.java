package es.com.inetum.elementos.modelo.test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import es.com.inetum.elementos.modelo.ElementoFactory;
import es.com.inetum.elementos.modelo.Papel;
import es.com.inetum.elementos.modelo.Piedra;
import es.com.inetum.elementos.modelo.Tijeras;

class ElementosFactoryTest {
	
	// lote de pruebas
	Piedra piedra;
	Papel papel;
	Tijeras tijeras;
	
	@Before
	public void setUp() throws Exception{
		// antes de cada testeo
		piedra  = new Piedra();
		papel   = new Papel();
		tijeras = new Tijeras();
	}
	
	@After
	public void tearDown() throws Exception{
		// despues de cada testeo
		piedra   = null;
		papel    = null;
		tijeras  = null;
	}
	

	@Test
	void testCompararPiedraConTijeras() {
		assertEquals(1,piedra.comparar(tijeras));
	}
	
	@Test
	void testCompararPiedraConPapel() {
		assertEquals(-1,piedra.comparar(papel));
	}
	 
	@Test
	void testGetInstancePiedra() {
		assertTrue(ElementoFactory.getInstance(0) instanceof Piedra);
	}
	
	@Test
	void testGetInstancePapel() {
		assertTrue(ElementoFactory.getInstance(1) instanceof Papel);
	}
	
	@Test
	void testGetInstanceTijeras() {
		assertTrue(ElementoFactory.getInstance(2) instanceof Tijeras);
	}

}
