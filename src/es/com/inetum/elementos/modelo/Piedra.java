package es.com.inetum.elementos.modelo;

public class Piedra extends ElementoFactory {

	public Piedra() {
		super("piedra", 0);
	}

	@Override
	public int comparar(ElementoFactory pElem) {
		int numero    = pElem.getNumero();
		int resultado = 0;
		if(numero == TIJERAS) {
			descripcionResultado = "piedra le gan� a tijeras";
			resultado = 1;
		}
		else if(numero == PAPEL) {
			descripcionResultado = "piedra perdi� con papel";
			resultado = -1;
		}
			
		else {
			descripcionResultado = "empate";
			resultado = 0;
		}
			
				
		return resultado;
		
	}

}
