package es.com.inetum.elementos.modelo;

public class Tijeras extends ElementoFactory {

	public Tijeras() {
		super("tijeras", 2);
	}

	@Override
	public int comparar(ElementoFactory pElem) {
		int numero                  = pElem.getNumero();
		int resultado               = 0;
		if(numero == PAPEL) {
			resultado = 1;
		    descripcionResultado = "tijeras le gan� a papel";
		}
		else if(numero == PIEDRA) {
			resultado = -1;
			descripcionResultado = "tijeras pierde con piedra";
		}
		else {
			resultado = 0;
			descripcionResultado = "empate";
		}
				
		return resultado;
		
	}

}
